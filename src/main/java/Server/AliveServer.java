package Server;

import Utils.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 心跳服务，非Netty服务
 * 仅仅用于检测某个Netty服务的状态
 */
public class AliveServer {

    public static Logger logger = LoggerFactory.getLogger(AliveServer.class);
    static ThreadPoolExecutor threadPool = ThreadPoolUtil.getFixedThreadPool(1); // 检查心跳用一个线程就行

    public static void startCheckNettyServer(NettyHttpServer nettyHttpServer){
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                while (true){
                    String status = nettyHttpServer.getChannelStatus();
                    logger.info("NettyServer Status is {},port is {}", status, nettyHttpServer.getPort());
                    try {
                        Thread.sleep(5 * 1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
    }
}
