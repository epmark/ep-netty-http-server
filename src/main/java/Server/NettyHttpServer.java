package Server;

import handlers.inbound.EpHttpServerHandler;
import handlers.outbound.WriteRespHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 使用Netty生成一个http的服务端
 * 参数为启动的端口
 */
public class NettyHttpServer {
    public static Logger logger = LoggerFactory.getLogger(NettyHttpServer.class);
    private int port = 9999;

    public NettyHttpServer(int port) {
        this.port = port;
    }
    Channel channel;

    public void start() {
        //boss线程池，work线程池
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);//一般监听几个端口就用多少线程
        EventLoopGroup workGroup = new NioEventLoopGroup(8);//此处使用cpu核数*2

        try {
            //创建启动类， serverboot
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 256)  //应用于work线程
                    .childOption(ChannelOption.SO_KEEPALIVE, true)  //应用于child线程
                    .childHandler(new ChannelInitializer<SocketChannel>() {

                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline().addLast(new HttpResponseEncoder()); //编码应答的报文
                            //channel.pipeline().addLast(new EpHttpServerHandler());
                            channel.pipeline().addLast(new HttpRequestDecoder()); //解析接入的报文
                            channel.pipeline().addLast(new HttpObjectAggregator(4*1024*1024));//4M
                            channel.pipeline().addLast(new EpHttpServerHandler());

                            //channel.pipeline().addLast(new WriteRespHandler());
                        }
                    });
            ChannelFuture future = serverBootstrap.bind(port).sync(); //启动并同步等待启动成功
            channel = future.channel();
            if(future.isSuccess()){
                logger.info("nettyHttpServer start success , port {}" , port);
                logger.info("nettyHttpServer status is {}" , getChannelStatus());
            }

            //future.channel().closeFuture().sync(); //对关闭进行监听
            //logger.info("nettyHttpServer close success , port {}" , port);


        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }finally {
            //bossGroup.shutdownGracefully();
            //workGroup.shutdownGracefully();
        }

    }

    public String getChannelStatus(){
        if(channel!=null) {
            if(channel.isActive()){
                return "active";
            }
            if(channel.isOpen()){
                return "open";
            }
            if(channel.isRegistered()){
                return "registered";
            }
            if(channel.isWritable()){
                return "writable";
            }
            return "unknown";
        }else{
            return "notStart";
        }
    }


    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
