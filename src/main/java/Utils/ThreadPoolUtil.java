package Utils;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 可以使用此工具类生成一个线程池
 */
public class ThreadPoolUtil {

    //static ThreadPoolExecutor threadPoolExecutor  ;
    //static int coreSize = 4;
    //static int maxSize = 4;  //固定使用4个线程
    static int keepAliveTime = 30;

    /**
     * 返回一个固定大小的线程池，且不使用队列，超过这个线程池大小就拒绝
     * @param poolSize
     * @return
     */
    public static ThreadPoolExecutor getFixedThreadPool(int poolSize){

        RejectedExecutionHandler rejectedHander = new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                throw new RejectedExecutionException("task fail" );
            }
        };
        ThreadFactory threadFactory = new ThreadFactory() {
            private String prefix = "async-task-";
            private AtomicInteger threadNumer = new AtomicInteger(1);
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(null,r,prefix + threadNumer.getAndIncrement());
                return thread;
            }
        };
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(poolSize,poolSize,keepAliveTime, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>(),  //不想使用队列
                threadFactory,rejectedHander);

        return threadPoolExecutor;
    }
}
