import Server.AliveServer;
import Server.NettyHttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 程序主入口
 */
public class PojectMain {
    public static Logger logger = LoggerFactory.getLogger(PojectMain.class);

    public static void main(String[] args) {

        NettyHttpServer nettyHttpServer = new NettyHttpServer(8090);
        nettyHttpServer.start();
        AliveServer.startCheckNettyServer(nettyHttpServer);
        logger.info("nettyHttpServer main finish" );

    }
}
