package BusinessServer;



import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * 假设这是一个业务处理类
 */
public class BizServer {
    public static Logger logger = LoggerFactory.getLogger(BizServer.class);
    public void handleBiz(ChannelHandlerContext ctx, FullHttpMessage fullHttpMessage) throws UnsupportedEncodingException {
        logger.info("系统现在将根据请求的报文进行业务处理");
        String contentType = fullHttpMessage.headers().getAsString(HttpHeaderNames.CONTENT_TYPE);
        logger.info("contentType is : {}" , contentType);

        int readablebytes = fullHttpMessage.content().readableBytes();
        byte[] reqByte = new byte[readablebytes];
        fullHttpMessage.content().readBytes(reqByte);
        String reqString = new String(reqByte, StandardCharsets.UTF_8);
        logger.info("reqString is : {}" , reqString);

        logger.info("业务处理完了" );


        String respMsg = "I have Receive your request,我已经收到了你的请求并处理完成。谢谢";
        logger.info("现在我返回给前端一个应答,应答报文是: {}", respMsg );

        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.wrappedBuffer(respMsg.getBytes("utf-8")));

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain;charset=UTF-8");
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);

        ctx.writeAndFlush(response);



    }
}
