package Client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 原本预计实现一个客户端demo，但是客户端准备放在另一个工程，此类暂时不用
 */
public class EchoClient {
    public static Logger logger = LoggerFactory.getLogger(EchoClient.class);

    public static void main(String[] args) {
        String userName = "jerry";
        logger.info("user name is {}", userName);
        logger.debug("user name is {}", userName);
    }
}
