package handlers.inbound;

import BusinessServer.BizServer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * Netty服务流水线中的一个入站Handler,用于接收报文
 */
@ChannelHandler.Sharable
public class EpHttpServerHandler extends ChannelInboundHandlerAdapter {
    public static Logger logger = LoggerFactory.getLogger(EpHttpServerHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("有请求进来了");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        FullHttpMessage fullHttpMessage = (FullHttpMessage) msg;
        //logger.info("收到客户端" + ctx.channel().remoteAddress() + "发送的消息："
        //       + byteBuf.toString(CharsetUtil.UTF_8));
        logger.info("收到客户端" + ctx.channel().remoteAddress() + "发送的Http消息：" + "\r\nHTTP header : " +
                fullHttpMessage.headers() + "\r\nHTTP body: " + fullHttpMessage.content().toString(CharsetUtil.UTF_8));

       new BizServer().handleBiz(ctx,fullHttpMessage);

    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        logger.info("读完了一段消息");
        //发送消息给客户端
       // String respRandomMsg = "I am the random Msg, and msg Id is " + new Random().nextInt(200);
        // logger.info("现在开始写应答,应答消息是{}" , respRandomMsg);
        //ctx.writeAndFlush(Unpooled.copiedBuffer(respRandomMsg, CharsetUtil.UTF_8));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}
